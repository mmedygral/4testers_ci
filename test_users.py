from users import User

def test_if_user_has_been_created():
    user = User('example@vp.pl', 'password123')

    assert user.email == 'example@vp.pl'
    assert user.password == 'password123'
    assert not user.logged

def test_if_user_is_logged_in():
    user = User('example@vp.pl', 'password123')
    user.login('password123')
    assert user.logged


def test_if_user_is_logged_in_not_correct():
    user = User('example@vp.pl', 'password123')
    user.login('password12')
    assert not user.logged


def test_if_user_is_logged_out():
    user = User('example@vp.pl', 'password123')
    user.login('password123')
    user.logout()
    assert not user.logged
