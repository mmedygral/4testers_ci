class User:
    def __init__(self, email, password):
        self.email = email
        self.password = password
        self.logged = False

    def login(self, password):
        if password == self.password:
            self.logged = True

    def logout(self):
        self.logged = False